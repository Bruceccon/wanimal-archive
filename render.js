
let renderDate = (post) => post.date
let renderUrlWithSlug = (post, anchor) => post['url-with-slug'] + anchor || ''
let renderUrl = (post, anchor) => post.url + anchor || ''
let renderRegularTitle = (post) => post['regular-title'] ? `<h2><a href="${renderUrlWithSlug(post)}">${post['regular-title']}</a></h2>` : ""
let renderNoteCount = (post) => post['note-count']
                                                                       
                                                                       
let renderRegularBody = (post) => post['regular-body'] || ""
let renderTags = (post) => posts['tags'] ? `<div class="tags">
  ${posts.tags.map(t => `<a href="http://wanimal1983.org/tagged/${t}">#${t}</a>`)}      
  </div>` : ''
let renderMeta = (post) => `
      <div class="meta">
        <div class=like-button><div class=like-button><a class=like_button href=#like><svg fill=#ccc fill=#000000 height=16 viewBox="0 0 19 16"width=16 xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink><path d="M14.0425097,0.000920262799 C16.1435097,-0.0400797372 18.8835097,1.28192026 18.9635097,5.36992026 C19.0525097,9.95492026 15.1985097,13.3079203 9.48350967,16.2089203 C3.76650967,13.3079203 -0.0874903349,9.95492026 0.00150966509,5.36992026 C0.0815096651,1.28192026 2.82150967,-0.0400797372 4.92250967,0.000920262799 C7.02450967,0.0419202628 8.87050967,2.26592026 9.46950967,2.92792026 C10.0945097,2.26592026 11.9405097,0.0419202628 14.0425097,0.000920262799 Z"></path></svg></a></div></div><div class=reblog-button><a class=reblog_button href=#reblog><svg fill=#ccc height=16 viewBox="0 0 21 21"width=16 xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink><path d="M5.01092527,5.99908429 L16.0088498,5.99908429 L16.136,9.508 L20.836,4.752 L16.136,0.083 L16.1360004,3.01110845 L2.09985349,3.01110845 C1.50585349,3.01110845 0.979248041,3.44726568 0.979248041,4.45007306 L0.979248041,10.9999998 L3.98376463,8.30993634 L3.98376463,6.89801007 C3.98376463,6.20867902 4.71892527,5.99908429 5.01092527,5.99908429 Z"></path><path d="M17.1420002,13.2800293 C17.1420002,13.5720293 17.022957,14.0490723 16.730957,14.0490723 L4.92919922,14.0490723 L4.92919922,11 L0.5,15.806 L4.92919922,20.5103758 L5.00469971,16.9990234 L18.9700928,16.9990234 C19.5640928,16.9990234 19.9453125,16.4010001 19.9453125,15.8060001 L19.9453125,9.5324707 L17.142,12.203"></path></svg></a></div>


         <div class="datenotes"><a href="${renderUrlWithSlug(post)}">${renderDate(post)}</a> / <a href="${renderUrlWithSlug(post, '#notes')}">${renderNoteCount(post)}</a></div>
      </div>
`

let subImageUrl = (p) => {
  let segs = p.split('/')
  let fname = segs[segs.length - 1]
  return `https://media.githubusercontent.com/media/plantvsbirds/wanimal-archive/gh-pages/img/${fname}?raw=true`
}

let renderImageObject = (p) => {
  return subImageUrl(p["photo-url-1280"])
}


let renderPhotoSets = (post) => post.photos ? `
  <div class="photoset-grid" data-layout="11" style="width: 100%;">
    ${post.photos.map(p => 
      `<div
            class="photoset-row cols-1"
            style="">
          <a   
              href="${renderImageObject(p)}"
              class="photoset-cell highres-link"
              rel=""
              style="float: left; display: block; line-height: 0; box-sizing: border-box; width: 100%;"
          >
            <img
              src="${renderImageObject(p)}"
              data-highres="${renderImageObject(p)}"
              style="width: 100%; height: auto; margin-top: 0px;" />
          </a>
      </div>`).join('')}
  </div>` : ''

let renderPhotoCaption = (post) => post['photo-caption'] ? `
  <div class="photoCaption">${post['photo-caption']}</div>
` : ''

let renderMedia = (post) => post['photo-url-1280'] ? `
  <div class="media">
    <a href="${renderUrl(post)}">
      <img
        src="${subImageUrl(post['photo-url-1280'])}"
        alt="${post['photo-caption']}"
      />
    </a>
  </div>` : ''

let render = (post) => `
  <div class="post">
    <div class="photo-posts">
        ${renderMedia(post)}    
        <div class="photo-sets">
            ${renderPhotoSets(post)}
        </div>
        ${renderPhotoCaption(post)}
    </div>

    <div class="post regular-posts">
      ${renderRegularTitle(post)}
      ${renderRegularBody(post)}
    </div>

    <div class="post-foot">
      ${renderTags(post)}
      ${renderMeta(post)}
    </div>
  </div>
`
let getPosts = (callback) => {
  var request = new XMLHttpRequest();
  request.open('GET', 'https://raw.githubusercontent.com/plantvsbirds/wanimal-archive/master/small_posts.json', true);

  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      // Success!
      callback(this.response);
    } else {
      // We reached our target server, but it returned an error
    }
  };
  request.onerror = function() {
    // There was a connection error of some sort
  };
  request.send();
}

let ready = (fn) => {
  if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

let renderPosts = (start, end, source) => {
  let posts = source.slice(start, end)
  let container = document.getElementById('post-container')
  container.innerHTML = posts.map((p) => render(p)).join('')
}

ready(() =>
  getPosts((posts) => {
    window.posts = JSON.parse(posts)
    renderPosts(0, 100, window.posts)
  }))
